package jonathan.aotearoa.numerals;

public class SimpleIntegerConverterTest extends IntegerConverterTest {

    @Override
    public IntegerConverter newIntegerConverter() {
        return new SimpleIntegerConverter();
    }
}