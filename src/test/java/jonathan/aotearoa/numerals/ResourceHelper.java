package jonathan.aotearoa.numerals;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSortedMap;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Helper class for reading test data
 */
public class ResourceHelper {

    private static final String NUMERAL_VALUES_RESOURCE = "numeral-values.txt";
    private static final Pattern LINE_PATTERN = Pattern.compile("\\d+=[IVXLCDM]+");

    /**
     * Reads the the {@code numeral-values.txt} test resource
     *
     * @return An {@code ImmutableSortedMap} containing numerals keyed by their integer value
     */
    public static ImmutableSortedMap<Integer, String> loadNumeralValues() {
        try {
            final URL url = Thread.currentThread().getContextClassLoader().getResource(NUMERAL_VALUES_RESOURCE);
            assertNotNull("url cannot be null", url);
            final Path path = Paths.get(url.toURI());
            final ImmutableSortedMap.Builder<Integer, String> builder = ImmutableSortedMap.naturalOrder();
            final Splitter splitter = Splitter.on('=');
            Files.lines(path).forEach(line -> {
                assertTrue("invalid line: " + line, LINE_PATTERN.matcher(line).matches());
                final List<String> tokens = splitter.splitToList(line);
                final Integer intValue = Integer.parseInt(tokens.get(0));
                final String numerals = tokens.get(1);
                builder.put(intValue, numerals);
            });
            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException("Error loading numeral values from " + NUMERAL_VALUES_RESOURCE, e);
        }
    }
}