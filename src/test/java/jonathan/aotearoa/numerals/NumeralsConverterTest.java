package jonathan.aotearoa.numerals;

import org.junit.Before;
import org.junit.Test;

import java.util.SortedMap;

import static jonathan.aotearoa.numerals.ResourceHelper.loadNumeralValues;
import static org.junit.Assert.assertEquals;

public abstract class NumeralsConverterTest {

    private static final SortedMap<Integer, String> NUMERAL_VALUES = loadNumeralValues();
    private NumeralsConverter converter;

    @Before
    public void before() {
        converter = newNumeralsConverter();
    }

    @Test(expected = NullPointerException.class)
    public void nullThrowsNullPointerException() {
        converter.toInteger(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyStringThrowsIllegalArgumentException() {
        converter.toInteger("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidRomanNumeralsThrowsIllegalArgumentException() {
        converter.toInteger("IMV");
    }

    @Test
    public void testOneToFiveThousand() {
        NUMERAL_VALUES.entrySet().forEach(entry -> {
            final int intValue = entry.getKey();
            final String numerals = entry.getValue();
            assertEquals(intValue, converter.toInteger(numerals));
        });
    }

    protected abstract NumeralsConverter newNumeralsConverter();
}