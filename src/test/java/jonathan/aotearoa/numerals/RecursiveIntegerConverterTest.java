package jonathan.aotearoa.numerals;

public class RecursiveIntegerConverterTest extends IntegerConverterTest {

    @Override
    public IntegerConverter newIntegerConverter() {
        return new RecursiveIntegerConverter();
    }
}