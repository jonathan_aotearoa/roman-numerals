package jonathan.aotearoa.numerals;

public class RecursiveNumeralsConverterTest extends NumeralsConverterTest {

    @Override
    protected NumeralsConverter newNumeralsConverter() {
        return new RecursiveNumeralsConverter();
    }
}