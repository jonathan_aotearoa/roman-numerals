package jonathan.aotearoa.numerals;

import org.junit.Before;
import org.junit.Test;

import java.util.SortedMap;

import static jonathan.aotearoa.numerals.ResourceHelper.loadNumeralValues;
import static org.junit.Assert.assertEquals;

/**
 * Base class for {@link IntegerConverter} implementation tests
 */
public abstract class IntegerConverterTest {

    private static final SortedMap<Integer, String> NUMERAL_VALUES = loadNumeralValues();
    private IntegerConverter converter;

    @Before
    public void before() {
        converter = newIntegerConverter();
    }

    @Test(expected = IllegalArgumentException.class)
    public void zeroThrowsIllegalArgumentException() {
        converter.toRomanNumerals(0);
    }

    @Test
    public void testOneToFiveThousand() {
        NUMERAL_VALUES.entrySet().forEach(entry -> {
            final int intValue = entry.getKey();
            final String numerals = entry.getValue();
            assertEquals(numerals, converter.toRomanNumerals(intValue));
        });
    }

    public abstract IntegerConverter newIntegerConverter();
}