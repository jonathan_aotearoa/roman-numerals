package jonathan.aotearoa.numerals;

/**
 * Defines the contract for converting an integer to Roman numerals
 */
public interface IntegerConverter {

    /**
     * Converts an integer to Roman numerals
     *
     * @param intValue the integer to convert
     * @return the integer in Roman numeral notation
     */
    public String toRomanNumerals(int intValue);
}