package jonathan.aotearoa.numerals;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * The simplest implementation, although a little verbose.
 */
public class SimpleIntegerConverter implements IntegerConverter {

    private static final String ONE_THOUSAND = "M";
    private static final String NINE_HUNDRED = "CM";
    private static final String FIVE_HUNDRED = "D";
    private static final String FOUR_HUNDRED = "CD";
    private static final String ONE_HUNDRED = "C";
    private static final String NINETY = "XC";
    private static final String FIFTY = "L";
    private static final String FORTY = "XL";
    private static final String TEN = "X";
    private static final String NINE = "IX";
    private static final String FIVE = "V";
    private static final String FOUR = "IV";
    private static final String ONE = "I";

    @Override
    public String toRomanNumerals(final int intValue) {
        checkArgument(intValue > 0, "intValue must be greater than zero");

        final StringBuilder numerals = new StringBuilder();
        int remainder = intValue;

        while (remainder >= 1000) {
            numerals.append(ONE_THOUSAND);
            remainder -= 1000;
        }

        // subtractive notation
        if (remainder >= 900) {
            numerals.append(NINE_HUNDRED);
            remainder -= 900;
        }

        if (remainder >= 500) {
            numerals.append(FIVE_HUNDRED);
            remainder -= 500;
        }

        // subtractive notation
        if (remainder >= 400) {
            numerals.append(FOUR_HUNDRED);
            remainder -= 400;
        }

        while (remainder >= 100) {
            numerals.append(ONE_HUNDRED);
            remainder -= 100;
        }

        // subtractive notation
        if (remainder >= 90) {
            numerals.append(NINETY);
            remainder -= 90;
        }

        if (remainder >= 50) {
            numerals.append(FIFTY);
            remainder -= 50;
        }

        // subtractive notation
        if (remainder >= 40) {
            numerals.append(FORTY);
            remainder -= 40;
        }

        while (remainder >= 10) {
            numerals.append(TEN);
            remainder -= 10;
        }

        // subtractive notation
        if (remainder >= 9) {
            numerals.append(NINE);
            remainder -= 9;
        }

        if (remainder >= 5) {
            numerals.append(FIVE);
            remainder -= 5;
        }

        // subtractive notation
        if (remainder >= 4) {
            numerals.append(FOUR);
            remainder -= 4;
        }

        while (remainder >= 1) {
            numerals.append(ONE);
            remainder--;
        }

        return numerals.toString();
    }
}