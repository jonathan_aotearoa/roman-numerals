package jonathan.aotearoa.numerals;

import com.google.common.collect.ImmutableSortedMap;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Uses recursion to reduce the amount of code seen in {@link SimpleIntegerConverter}.
 */
public class RecursiveIntegerConverter implements IntegerConverter {

    private static final ImmutableSortedMap<Integer, String> NUMERALS = ImmutableSortedMap.<Integer, String>reverseOrder()
            .put(1000, "M")
            .put(900, "CM")
            .put(500, "D")
            .put(400, "CD")
            .put(100, "C")
            .put(90, "XC")
            .put(50, "L")
            .put(40, "XL")
            .put(10, "X")
            .put(9, "IX")
            .put(5, "V")
            .put(4, "IV")
            .put(1, "I")
            .build();

    @Override
    public String toRomanNumerals(int intValue) {
        checkArgument(intValue > 0, "intValue must be greater than zero");

        final StringBuilder sb = new StringBuilder();
        appendNumerals(sb, NUMERALS.firstKey(), intValue);
        return sb.toString();
    }

    /**
     * Recursive method for appending numerals
     *
     * @param sb           the StringBuilder to append to
     * @param numeralValue the current numeral value
     * @param remainder    the current remainder
     */
    private void appendNumerals(final StringBuilder sb, final Integer numeralValue, final int remainder) {
        if (remainder >= numeralValue) {
            sb.append(NUMERALS.get(numeralValue));
            appendNumerals(sb, numeralValue, remainder - numeralValue);
        } else {
            final Integer nextNumeralValue = NUMERALS.ceilingKey(numeralValue - 1);
            if (nextNumeralValue != null) {
                appendNumerals(sb, nextNumeralValue, remainder);
            }
        }
    }
}