package jonathan.aotearoa.numerals;

/**
 * Defines the contract for converting Roman numerals to an integer
 */
public interface NumeralsConverter {

    /**
     * Converts the specified Roman numerals to an integer
     *
     * @param romanNumerals the Roman numerals
     * @return the Roman numerals as in integer
     */
    int toInteger(String romanNumerals);
}