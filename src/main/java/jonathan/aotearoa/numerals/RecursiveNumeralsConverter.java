package jonathan.aotearoa.numerals;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 */
public class RecursiveNumeralsConverter implements NumeralsConverter {

    private static final String NUMERALS_REGEX = "M{0,5}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})";
    private static final Pattern NUMERALS_PATTERN = Pattern.compile(NUMERALS_REGEX);

    private static final ImmutableList<Numeral> NUMERALS = ImmutableList.of(
            new Numeral("M", 1000),
            new Numeral("CM", 900),
            new Numeral("D", 500),
            new Numeral("CD", 400),
            new Numeral("C", 100),
            new Numeral("XC", 90),
            new Numeral("L", 50),
            new Numeral("XL", 40),
            new Numeral("X", 10),
            new Numeral("IX", 9),
            new Numeral("V", 5),
            new Numeral("IV", 4),
            new Numeral("I", 1)
    );

    @Override
    public int toInteger(final String romanNumerals) {
        checkNotNull(romanNumerals, "rommanNumerals cannot be null");
        checkArgument(StringUtils.isNotEmpty(romanNumerals), "romanNumerals cannot be empty");
        if (NUMERALS_PATTERN.matcher(romanNumerals).matches()) {
            return processNumeral(romanNumerals, 0);
        }
        throw new IllegalArgumentException("Invalid Roman numerals: " + romanNumerals);
    }

    /**
     * Un-optimised recursive method for processing remaining numerals
     *
     * @param remainingNumerals the unprocessed, remaining, numerals
     * @param total             the total, integer, value of the numerals that have currently been processed
     * @return the numerals as an integer value
     */
    private int processNumeral(final String remainingNumerals, final int total) {
        for (int i = 0, n = NUMERALS.size(); i < n; i++) {
            final Numeral numeral = NUMERALS.get(i);
            if (remainingNumerals.startsWith(numeral.numerals)) {
                final int newTotal = total + numeral.intValue;
                final int startIndex = numeral.numerals.length();
                final String newRemainingNumerals = remainingNumerals.substring(startIndex);
                return processNumeral(newRemainingNumerals, newTotal);
            }
        }
        return total;
    }

    private static final class Numeral implements Comparable<Numeral> {

        final String numerals;
        final Integer intValue;

        private Numeral(final String numerals, final Integer intValue) {
            this.numerals = checkNotNull(numerals, "numerals cannot be null");
            this.intValue = checkNotNull(intValue, "intValue cannot be null");
        }

        @Override
        public int compareTo(final Numeral other) {
            return intValue.compareTo(other.intValue);
        }

        @Override
        public boolean equals(final Object other) {
            if (this == other) return true;
            if (other == null || getClass() != other.getClass()) return false;
            Numeral numeral = (Numeral) other;
            return numerals.equals(numeral.numerals);
        }

        @Override
        public int hashCode() {
            return numerals.hashCode();
        }
    }
}