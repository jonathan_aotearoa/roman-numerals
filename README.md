# Roman Numerals

## Converting an integer value to Roman numerals

It's surprisingly simply when you think about it!

### Testing

Each ``Converter`` implementation is tested for integer values in the range 0 to 5000 inclusive.
Passing a value < 1 should throw an ``IllegalArgumentException``.
The results for values in the range 1 - 5000 inclusive are compared against the values defined in ``numeral-values.txt``.
